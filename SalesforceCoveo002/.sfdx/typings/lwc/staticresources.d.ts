declare module "@salesforce/resourceUrl/CoveoJsSearchQV" {
    var CoveoJsSearchQV: string;
    export default CoveoJsSearchQV;
}
declare module "@salesforce/resourceUrl/SiteSamples" {
    var SiteSamples: string;
    export default SiteSamples;
}
declare module "@salesforce/resourceUrl/community" {
    var community: string;
    export default community;
}
declare module "@salesforce/resourceUrl/image" {
    var image: string;
    export default image;
}
declare module "@salesforce/resourceUrl/jQuery_3_3_1_min" {
    var jQuery_3_3_1_min: string;
    export default jQuery_3_3_1_min;
}
