({
generateSearchToken: function(component, event, helper) {
        console.log('>>>>>> generateSearchToken');
 
        var deferred = event.getParam('deferred');
        var action = component.get('c.getToken');
        action.setParams({
            searchHub: component.get('v.searchHub')
        });
 
        action.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS') {
                deferred.resolve({
                    searchToken : response.getReturnValue()
                })
            }
        });
        
        $A.enqueueAction(action);
    },
    onInterfaceContentLoaded: function(component, event, helper) {
        console.log('>>>>>> onInterfaceContentLoaded');
 
        var searchInterface = Coveo.$('#coveoSearch');
        
               Coveo.$(searchInterface).closest('[data-region-name=header]').width('100%');
        
        searchInterface.on('preprocessResults', function(e, args) {
            if (args && args.results && args.results.results) {
                args.results.results.forEach(function(result) {
                    // Remove filename extension
                    if (result.raw && !result.title && result.raw.filename) {
                        result.title = result.raw.filename.replace(/\.[^/.]+$/, '');
                    }
                    // Set a flag (additional field) if website is HTTP or fanniemae.com (blocking IFrame)                    
                    if (result.raw && result.raw.sourcetype === 'Web') {
                        if (result.raw.clickableuri.indexOf('http://') == 0 || result.raw.clickableuri.indexOf('https://www.fanniemae.com') == 0) {
                            result.raw.webwithoutquickview = true;
                        }
                    }
                    
                });
            }
        });
        
        searchInterface.on('deferredQuerySuccess', function(e) {
            var results = document.querySelectorAll('.CoveoResult');
            if (results && results.length > 0) {
                results.forEach(function (result) {
                    var parentQuickview = result.querySelector('.CoveoQuickview');
                            if(parentQuickview) {
                        parentQuickview.addEventListener('click', function(e) {
                            var elem = e.target;
                            while ((elem = elem.parentElement) && !elem.classList.contains('coveo-result-row')) { };
                           var childQuickviewElement = elem.children[1].querySelector('.CoveoQuickview');
                            if(childQuickviewElement) {
                                var childQuickview = Coveo.get(childQuickviewElement, Coveo.Quickview);
                                childQuickview.open();
                                e.stopImmediatePropagation();
                            }
                        });
}
                });
            } else {
                var noResultsDiv = Coveo.$("#coveoSearch .coveo-query-summary-no-results-string");
                noResultsDiv.html('<p style="font-size: 2rem; color:#006199;">We apologize we couldn’t find what you are looking for. We have logged your search and are working to create content for this topic. Please contact your AE for immediate assistance. Thank you for choosing UWM, where we are younited to make lending easy</p>');
}
              });
        
/*
        searchInterface.on('deferredQuerySuccess', function(e) {
            var results = document.querySelectorAll('.CoveoResult');
            if (results && results.length > 0) {
                results.forEach(function (result) {
                    var parentQuickview = result.querySelector('.CoveoQuickview');
                    var tmp = parentQuickview;
                    while ((tmp = tmp.parentElement) && !tmp.classList.contains('coveo-result-row')) { };
                    var childQuickviewElement = tmp.children[1].querySelector('.CoveoQuickview');
                    if(!childQuickviewElement) {
                        parentQuickview.remove();
                    }
                            if(parentQuickview) {
                        parentQuickview.addEventListener('click', function(e) {
                            var elem = e.target;
                            while ((elem = elem.parentElement) && !elem.classList.contains('coveo-result-row')) { };
                            var childQuickviewElement = elem.children[1].querySelector('.CoveoQuickview');
                            if(childQuickviewElement) {
                                var childQuickview = Coveo.get(childQuickviewElement, Coveo.Quickview);
                                childQuickview.open();
                                e.stopImmediatePropagation();
                            }
                        });
}
                });
            }
              });
        */
 
        searchInterface.on('doneBuildingQuery', function(e, args) {
var query = args.queryBuilder.expression.parts.toString().trim();
var lendingUrl = component.get('v.lendingUrl') || 'https://ease.uwm.com';
/*            
            if (args.queryBuilder.expression.parts[0] != undefined) {
                if (args.queryBuilder.expression.parts[0].split("\"").length - 1 <= 0 && args.queryBuilder.expression.parts[0].split(" ").length - 1 <= 0) {
                    // One word only with no quotes
                }
                else if(args.queryBuilder.expression.parts[0].split("\"").length - 1 <= 0 && args.queryBuilder.expression.parts[0].split(" ").length - 1 > 0) {
                    // sentence with no quotes
                    let initial = '(' + args.queryBuilder.expression.parts[0] + ')';
                    let addition = ' OR \"' + args.queryBuilder.expression.parts[0] + '\"';
                    args.queryBuilder.expression.parts[0] = initial + addition;
                }
                else if (args.queryBuilder.expression.parts[0].split("\"").length - 1 > 0 && args.queryBuilder.expression.parts[0].split(" ").length - 1 > 0) {
                    // sentence with one or many pair of quotes
                }
            }
*/            
 
if(query.length === 10 && !Number.isNaN(Number.parseInt(query)) && (window.location.hash.indexOf('ln=false') === -1))
            {
args.cancel = true;
try {
var xhr = helper.createCORSRequest('POST', lendingUrl + '/Lending/Origination/LoanApplication/AskLockMyLoan');
if (!xhr) {
throw new Error('CORS not supported');
}
xhr.withCredentials = true;
 
xhr.onload = function () {
try {
var data = JSON.parse(xhr.responseText);
if(data.IsSuccess === false) {
helper.failLoanSearch(args.queryBuilder.expression.parts.toString());
} else {
var url = lendingUrl + '/Lending/Origination/LoanApplication/Index?DatabaseId='+ data.databaseId +'&LoanId=' + data.loanId;
window.location.href = url;
}
} catch(e) {
console.log(e);
helper.failLoanSearch(args.queryBuilder.expression.parts.toString());
}
};
 
xhr.onerror = function (e) {
console.log(e);
helper.failLoanSearch(args.queryBuilder.expression.parts.toString());
};
 
var fd = new FormData();
fd.append('LoanNumber', query);
 
xhr.send(fd);
 
} catch (e) {
console.log(e);
helper.failLoanSearch(args.queryBuilder.expression.parts.toString());
}
 
}
        });
        
        searchInterface.on('changeAnalyticsCustomData', function (e, args) {
            if(args.actionCause === 'searchFromLink' || args.actionCause === 'searchboxSubmit') {
                searchInterface.on('duringQuery', function (e, data) {
                    data.promise.done(function (results) {
                        args.metaObject.queryCorrections = (results.queryCorrections.length ? 'true' : 'false');
                    })
                });
                }
        });
}
})