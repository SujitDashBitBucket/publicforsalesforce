({
createCORSRequest: function (method, url) {
        var xhr = new XMLHttpRequest();
        if ("withCredentials" in xhr) {
            xhr.open(method, url, true);
        } else if (typeof XDomainRequest != "undefined") {
            xhr = new XDomainRequest();
            xhr.open(method, url);
        } else {
            xhr = null;
        }
        return xhr;
    },
 
failLoanSearch: function(query){
var url = window.location.hostname + '/s/search/All/Home/search#ln=false&t=ALL&q='+ query;
var urlEvent = $A.get("e.force:navigateToURL");
urlEvent.setParams({"url":url, "target": window});
urlEvent.fire();
}
})