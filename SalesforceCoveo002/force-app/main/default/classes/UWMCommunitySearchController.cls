public class UWMCommunitySearchController {
    // Check user license
    private static string getUserLicense() {
        String userId = UserInfo.getUserId();
 
        User currentUser = [
            Select Id, Profile.UserLicense.Name 
            From User 
            Where Id=:userId
        ];
 
        return currentUser.Profile.UserLicense.Name;
    }
 
    // Annotate the method so that it's accessible from the Lightning component
    @AuraEnabled
    public static string getToken(String searchHub) {
        // Filter out KB Articles based on user license 
        String filter = '((NOT @sfid) OR (';
        String internalUserFilter = '(@sfid @sfisvisibleinpkb==true) OR (@sfid @sfisvisibleincsp==true) OR (@sfid @sfisvisibleinapp==true)';
        String externalAuthUserFilter = '(@sfid @sfisvisibleinpkb==true) OR (@sfid @sfisvisibleincsp==true)';
        String publicUserFilter = '(NOT @sfid)';
/*
        String filter = '((NOT @documenttype==(article,articleattachment)) OR (';
        String internalUserFilter = '(@documenttype==(article,articleattachment) [[@foldfoldingfield] @sfisvisibleinpkb==true]) OR (@documenttype==(article,articleattachment) [[@foldfoldingfield] @sfisvisibleincsp==true]) OR (@documenttype==(article,articleattachment) [[@foldfoldingfield] @sfisvisibleinapp==true])';
        String externalAuthUserFilter = '(@documenttype==(article,articleattachment) [[@foldfoldingfield] @sfisvisibleinpkb==true]) OR (@documenttype==(article,articleattachment) [[@foldfoldingfield] @sfisvisibleincsp==true])';
        String publicUserFilter = '(NOT @documenttype==(article,articleattachment))';
*/
        String userLicense = getUserLicense();
        if (userLicense == 'Guest User License') {
            filter = publicUserFilter;
        } else if (userLicense == 'Customer Community Plus') {
            filter += externalAuthUserFilter;
            filter += '))';
        } else {
            filter += internalUserFilter;
            filter += '))';
        }
        
        // Generate a token using the Globals class provided by Coveo.
        return CoveoV2.Globals.generateSearchToken(new Map<String, Object> {
            'filter' => filter,
            'searchHub' => searchHub
        });
    }
}